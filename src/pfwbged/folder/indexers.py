from five import grok
from plone.indexer import indexer

from .link import ILink

from collective.dms.basecontent.dmsdocument import IDmsDocument

@indexer(IDmsDocument)
def object_direct_folders(obj, **kw):
    folders = []
    for id, item in obj.contentItems():
        if not ILink.providedBy(item):
            continue
        folders.append(str(item.folder.to_id))
    return folders
grok.global_adapter(object_direct_folders, name='object_direct_folders')


@indexer(IDmsDocument)
def object_folders(obj, **kw):
    folders = []
    for id, item in obj.contentItems():
        if not ILink.providedBy(item):
            continue
        folders.extend(item.folder.to_object.parent_folders_intid_tree())
    return [str(x) for x in folders]
grok.global_adapter(object_folders, name='object_folders')
