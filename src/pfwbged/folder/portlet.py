from zope import schema
from zope.formlib import form
from zope.interface import implements
from zope.component import getMultiAdapter
from plone.app.portlets.portlets import base
from plone.portlets.interfaces import IPortletDataProvider
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName

from . import _
from folder import IFolder


class INavigationPortlet(IPortletDataProvider):
    ''' '''


class Assignment(base.Assignment):
    implements(INavigationPortlet)
    title = _('Classifying Folder Nav')


class Renderer(base.Renderer):
    render = ViewPageTemplateFile('portlet.pt')

    def title(self):
        return _('Folder Navigation')

    @property
    def available(self):
        return IFolder.providedBy(self.context)


class AddForm(base.AddForm):
    form_fields = form.Fields(INavigationPortlet)
    label = _(u"Add Folder Navigation Portlet")
    description = _(u"This portlet display folder navigation elements.")

    def create(self, data):
        return Assignment()


class EditForm(base.EditForm):
    form_fields = form.Fields(INavigationPortlet)
    label = _(u"Edit Folder Navigation Portlet")
    description = _(u"This portlet display folder navigation elements.")
