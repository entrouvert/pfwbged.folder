from AccessControl import getSecurityManager
from zope.interface import implements
from five import grok
from zope import component
from zope import schema
from zope.interface import Interface
from zope.i18n import translate

from zc.relation.interfaces import ICatalog
from zope.app.intid.interfaces import IIntIds

from plone.dexterity.content import Container
from plone.uuid.interfaces import IUUID

from Acquisition import aq_parent

from plone.supermodel import model
from plone.app.layout.viewlets.interfaces import IBelowContentBody
import plone.app.contenttypes.interfaces
from plone.dexterity.interfaces import IDexterityContainer
from plone.app.contentlisting.interfaces import IContentListingObject

from plone import api

from plone.dexterity.browser.view import DefaultView

from collective.z3cform.rolefield.field import LocalRolesToPrincipals
from collective.dms.basecontent.widget import AjaxChosenMultiFieldWidget
from plone.autoform import directives as form
from plone.directives.form import default_value

import z3c.table.column



from pfwbged.collection.searchview import ResultsTable
from collective.dms.thesaurus.keywordsfield import ThesaurusKeywords

from collective.dms.basecontent.dmsdocument import IDmsDocument


from . import _

class IFolder(model.Schema):
    """ """
    treating_groups = LocalRolesToPrincipals(
        title=_(u"Can edit"),
        required=False,
        roles_to_assign=('Editor', 'Contributor',),
        value_type=schema.Choice(vocabulary=u'collective.dms.basecontent.treating_groups',)
    )
    form.widget(treating_groups=AjaxChosenMultiFieldWidget)

    recipient_groups = LocalRolesToPrincipals(
        title=_(u"Can view"),
        required=False,
        roles_to_assign=('Reader',),
        value_type=schema.Choice(vocabulary=u'collective.dms.basecontent.recipient_groups')
    )
    form.widget(recipient_groups=AjaxChosenMultiFieldWidget)

    keywords = ThesaurusKeywords(title=_(u'Keywords'), required=False)


@default_value(field=IDmsDocument['treating_groups'])
@default_value(field=IFolder['treating_groups'])
def canEditDefaultValue(data):
    try:
        return data.context.treating_groups
    except AttributeError:
        return []

@default_value(field=IDmsDocument['recipient_groups'])
@default_value(field=IFolder['recipient_groups'])
def canEditDefaultValue(data):
    try:
        return data.context.recipient_groups
    except AttributeError:
        return []


class Folder(Container):
    """ """
    implements(IFolder)

    def context_title(self):
        current_user = api.user.get_current()
        current_user_id = current_user.getId()
        members_folder = getattr(api.portal.get(), 'Members')
        if members_folder.get(current_user_id) == self:
            return translate(_(u'Home Folder'), context=self.REQUEST)
        else:
            return self.title

    def parent_folders(self):
        from .link import ILink
        parents = []
        sm = getSecurityManager()
        for id, item in self.contentItems():
            if not ILink.providedBy(item):
                continue
            if item.folder.to_object:
                if not sm.checkPermission('View', item.folder.to_object):
                    continue
                parents.append(item.folder.to_object)
        return parents

    def parent_folders_intid_tree(self, limit=5):
        intids = component.getUtility(IIntIds)
        l = [intids.getId(self)]
        if limit > 0:
            for parent in self.parent_folders():
                l.extend(parent.parent_folders_intid_tree(limit=limit-1))
        return l

    def child_folders_objects(self):
        from .link import ILink
        intids = component.getUtility(IIntIds)
        intid_catalog = component.getUtility(ICatalog)
        try:
            intid = intids.getId(self)
        except KeyError:
            return []

        folders = []
        sm = getSecurityManager()
        for item in intid_catalog.findRelations({
                'to_id': intid,
                'from_interfaces_flattened': ILink}):
            if item.isBroken():
                continue

            link = item.from_object
            if not sm.checkPermission('View', link):
                continue

            document = aq_parent(link)
            if IFolder.providedBy(document):
                folders.append(document)

        return sorted(folders, key=lambda x: x.Title().lower().strip())

    def child_folders(self):
        return [IContentListingObject(x) for x in self.child_folders_objects()]

    def intid(self):
        intids = component.getUtility(IIntIds)
        try:
            return intids.getId(self)
        except KeyError:
            return None


grok.templatedir('templates')
grok.context(IDexterityContainer)


from collective.dms.basecontent.browser import column

class ClassifiedItems:

    def documents(self):
        from .link import ILink
        if self.context.id == 'documents' and aq_parent(self.context).portal_type == 'Plone Site':
            # never return anything in the main documents folder
            return []

        intids = component.getUtility(IIntIds)
        intid_catalog = component.getUtility(ICatalog)
        try:
            intid = intids.getId(self.context)
        except KeyError:
            return []

        uuids = []
        sm = getSecurityManager()
        for item in intid_catalog.findRelations({
                'to_id': intid,
                'from_interfaces_flattened': ILink}):
            if item.isBroken():
                continue

            link = item.from_object
            if not sm.checkPermission('View', link):
                continue

            document = aq_parent(link)
            uuids.append(IUUID(document))

        portal_catalog = api.portal.get_tool('portal_catalog')
        children = portal_catalog.searchResults({'UID': uuids})

        return [IContentListingObject(x) for x in children]


#class FolderViewlet(grok.Viewlet, ClassifiedItems):
#    grok.context(plone.app.contenttypes.interfaces.IFolder)
#    grok.template('foldersviewlet')
#    grok.viewletmanager(IBelowContentBody)
#    grok.order(15)


class FolderView(DefaultView, ClassifiedItems):
    pass
